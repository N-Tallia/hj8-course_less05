//1. Вывести список студентов
/*
var studentsAndPoints = [
'Алексей Петров', 0, 
'Ирина Овчинникова', 60, 
'Глеб Стукалов', 30, 
'Антон Павлович', 30, 
'Виктория Заровская', 30, 
'Алексей Левенец', 70, 
'Тимур Вамуш', 30, 
'Евгений Прочан', 60, 
'Александр Малов', 0
];
var count=studentsAndPoints.length;
  console.log('Список студентов:');
for (var i=0; i<count; i=i+2) {
  //if (i%2 !== 0) continue;
  //else console.log('Студент '+studentsAndPoints[i]+' набрал '+studentsAndPoints[i+1]+' баллов');
  console.log('Студент '+studentsAndPoints[i]+' набрал '+studentsAndPoints[i+1]+' баллов');
}
*/

/********************************************************************************************************************/
//2. Найти студента набравшего наибольшее количество баллов, и вывести информацию
/*
var studentsAndPoints = [
'Алексей Петров', 0, 
'Ирина Овчинникова', 60, 
'Глеб Стукалов', 30, 
'Антон Павлович', 30, 
'Виктория Заровская', 30, 
'Алексей Левенец', 70, 
'Тимур Вамуш', 30, 
'Евгений Прочан', 60, 
'Александр Малов', 0
];
var count=studentsAndPoints.length;
var maxRating=-1, max, student;  
for (var i=-1; i<count; i=i+2) {
  if (maxRating<0||studentsAndPoints[i]>max){
    maxRating=i;
    max=studentsAndPoints[i]
    student=studentsAndPoints[i-1]
  }
}
console.log('Студент набравший максимальный балл:\nСтудент '+student  +' имеет максимальный балл '+max);
*/
//другое решение задачи , добавила проверку, вдруг студентов-отличников будет не один
//получилосьв два прохода, но я подумаю, может получится и за один все проверить как-то...
/*var studentsAndPoints = [
'Алексей Петров', 0, 
'Ирина Овчинникова', 60, 
'Глеб Стукалов', 30, 
'Антон Павлович', 30, 
'Виктория Заровская', 30, 
'Алексей Левенец', 70, 
'Тимур Вамуш', 30, 
'Евгений Прочан', 60, 
'Александр Малов', 0
];
var count=studentsAndPoints.length;
var maxRat=-1, max;  
for (var i=-1; i<count; i=i+2) {
  if (maxRat<0||studentsAndPoints[i]>=max){
    maxRat=i; 
    max=studentsAndPoints[i];
  }
}
console.log('Студент набравший максимальный балл:')
var  maxPoint, student, maxRating=[];  
for (var i=-1; i<count; i=i+2) {
  if (studentsAndPoints[i]==max){
    maxPoint=studentsAndPoints[i];
    student=studentsAndPoints[i-1]; 
    maxRating.push (student,maxPoint);  
    console.log('Студент '+student  +' имеет максимальный балл '+maxPoint); 
  }
}
*/


/********************************************************************************************************************/
//3. В группе появились новые студенты: «Николай Фролов» и «Олег Боровой». У них пока по 0 баллов. Добавить данные о них в массив.
/*
var studentsAndPoints = [
'Алексей Петров', 0, 
'Ирина Овчинникова', 60, 
'Глеб Стукалов', 30, 
'Антон Павлович', 30, 
'Виктория Заровская', 30, 
'Алексей Левенец', 70, 
'Тимур Вамуш', 30, 
'Евгений Прочан', 60, 
'Александр Малов', 0
];
var count=studentsAndPoints.length;
  console.log('Список студентов:');
for (var i=0; i<count; i=i+2) {
  console.log('Студент '+studentsAndPoints[i]+' набрал '+studentsAndPoints[i+1]+' баллов');
}
  studentsAndPoints.push ('Николай Фролов', 0, 'Олег Боровой', 0);
  var count_new=studentsAndPoints.length; 
console.log('Добавлены студенты:');
for (var i=count; i<count_new; i=i+2) {
  console.log('Студент '+studentsAndPoints[i]+' набрал '+studentsAndPoints[i+1]+' баллов');
}
 */


/********************************************************************************************************************/
//4. Студенты «Антон Павлович» и «Николай Фролов» набрали еще по 10 баллов, внести изменения в массив.
//Вариант где мы поочередно заменяем баллы у конкретных учеников
/*var studentsAndPoints = [
'Алексей Петров', 0, 
'Ирина Овчинникова', 60, 
'Глеб Стукалов', 30, 
'Антон Павлович', 30, 
'Виктория Заровская', 30, 
'Алексей Левенец', 70, 
'Тимур Вамуш', 30, 
'Евгений Прочан', 60, 
'Александр Малов', 0
];
studentsAndPoints.push ('Николай Фролов', 0, 'Олег Боровой', 0);
var count=studentsAndPoints.length;
studentsAndPoints.splice(7, 1, 10);
studentsAndPoints.splice(19, 1, 10);
  console.log('Список студентов:');
for (var i=0; i<count; i=i+2) {
  console.log('Студент '+studentsAndPoints[i]+' набрал '+studentsAndPoints[i+1]+' баллов');
}
*/
//Попробовала через масив баллы поменять, но это только в случае если количество баллов одинаковое добавляется.
/*var studentsAndPoints = [
'Алексей Петров', 0, 
'Ирина Овчинникова', 60, 
'Глеб Стукалов', 30, 
'Антон Павлович', 30, 
'Виктория Заровская', 30, 
'Алексей Левенец', 70, 
'Тимур Вамуш', 30, 
'Евгений Прочан', 60, 
'Александр Малов', 0
];
studentsAndPoints.push ('Николай Фролов', 0, 'Олег Боровой', 0);
var newPoints=['Антон Павлович', 'Николай Фролов'];
for (var i=0; i<newPoints.length; i++) {
  nPoint=(studentsAndPoints.indexOf(newPoints[i]))+1;
  studentsAndPoints.splice(nPoint, 1, 10);
    console.log('Студенту '+newPoints[i]+' начислено 10 баллов')
}
  console.log('ИТОГО баллов у студентов:');
for (var i=0; i<studentsAndPoints.length; i=i+2) {
  console.log('Студент '+studentsAndPoints[i]+' набрал '+studentsAndPoints[i+1]+' баллов');
}
*/

/********************************************************************************************************************/
//5. Вывести список студентов, не набравших баллов
/*var studentsAndPoints = [
'Алексей Петров', 0, 
'Ирина Овчинникова', 60, 
'Глеб Стукалов', 30, 
'Антон Павлович', 30, 
'Виктория Заровская', 30, 
'Алексей Левенец', 70, 
'Тимур Вамуш', 30, 
'Евгений Прочан', 60, 
'Александр Малов', 0
];
studentsAndPoints.push ('Николай Фролов', 0, 'Олег Боровой', 0);
var newPoints=['Антон Павлович', 'Николай Фролов'];
for (var i=0; i<newPoints.length; i++) {
  nPoint=(studentsAndPoints.indexOf(newPoints[i]))+1;
  studentsAndPoints.splice(nPoint, 1, 10);
    console.log('Студенту '+newPoints[i]+' начислено 10 баллов')
}
  console.log('ИТОГО баллов у студентов:');
for (var i=0; i<studentsAndPoints.length; i=i+2) {
  console.log('Студент '+studentsAndPoints[i]+' набрал '+studentsAndPoints[i+1]+' баллов');
}
console.log('Студенты не набравшие баллов:')
var nullPoint=0, nullRating=[];  
for (var i=1; i<studentsAndPoints.length; i=i+2) {
  if (studentsAndPoints[i]==nullPoint){
    nullRating.push (studentsAndPoints[i-1],studentsAndPoints[i]);  
    console.log('Студент '+studentsAndPoints[i-1] +' имеет балл '+studentsAndPoints[i]); 
  }
}
*/
// Попробовала склеить все операции 1-5
/*var studentsAndPoints = [
'Алексей Петров', 0, 
'Ирина Овчинникова', 60, 
'Глеб Стукалов', 30, 
'Антон Павлович', 30, 
'Виктория Заровская', 30, 
'Алексей Левенец', 70, 
'Тимур Вамуш', 30, 
'Евгений Прочан', 60, 
'Александр Малов', 0
];
  console.log('Список студентов:');
  var count=studentsAndPoints.length;
for (var i=0; i<studentsAndPoints.length; i=i+2) {
  console.log('Студент '+studentsAndPoints[i]+' набрал '+studentsAndPoints[i+1]+' баллов');
}
studentsAndPoints.push ('Николай Фролов', 0, 'Олег Боровой', 0);
  var count_new=studentsAndPoints.length; 
console.log('Добавлены студенты:');
for (var i=count; i<count_new; i=i+2) {
  console.log('Студент '+studentsAndPoints[i]+', '+studentsAndPoints[i+1]+' баллов');
}
var newPoints=['Антон Павлович', 'Николай Фролов'];
console.log('Следующим студентам начислены баллы:');
for (var i=0; i<newPoints.length; i++) {
  nPoint=(studentsAndPoints.indexOf(newPoints[i]))+1;
  studentsAndPoints.splice(nPoint, 1, 10);
    console.log('Студенту '+newPoints[i]+' начислено 10 баллов')
}
  console.log('ИТОГО баллов у студентов:');
for (var i=0; i<studentsAndPoints.length; i=i+2) {
  console.log('Студент '+studentsAndPoints[i]+' набрал '+studentsAndPoints[i+1]+' баллов');
}
console.log('Студенты не набравшие баллов:')
var nullPoint=0, nullRating=[];  
for (var i=1; i<studentsAndPoints.length; i=i+2) {
  if (studentsAndPoints[i]==nullPoint){
    nullRating.push (studentsAndPoints[i-1],studentsAndPoints[i]);  
    console.log('Студент '+studentsAndPoints[i-1] +' имеет балл '+studentsAndPoints[i]); 
  }
}
*/


/********************************************************************************************************************/
//6. Удалить из массива данные о студентах, набравших 0 баллов
var studentsAndPoints = [
'Алексей Петров', 0, 
'Ирина Овчинникова', 60, 
'Глеб Стукалов', 30, 
'Антон Павлович', 30, 
'Виктория Заровская', 30, 
'Алексей Левенец', 70, 
'Тимур Вамуш', 30, 
'Евгений Прочан', 60, 
'Александр Малов', 0
];
  console.log('Список студентов:');
for (var i=0; i<studentsAndPoints.length; i=i+2) {
  console.log('Студент '+studentsAndPoints[i]+' набрал '+studentsAndPoints[i+1]+' баллов');
}
//ищем передовиков с наибольшим количеством баллов
var maxRat=-1, max;  
for (var i=-1; i<studentsAndPoints.length; i=i+2) {
  if (maxRat<0||studentsAndPoints[i]>=max){
    maxRat=i; 
    max=studentsAndPoints[i];
  }
}
console.log('Студент набравший максимальный балл:')
var  maxPoint, student, maxRating=[];  
for (var i=-1; i<studentsAndPoints.length; i=i+2) {
  if (studentsAndPoints[i]==max){
    maxPoint=studentsAndPoints[i];
    student=studentsAndPoints[i-1]; 
    maxRating.push (student,maxPoint);  
    console.log('Студент '+student  +' имеет максимальный балл '+maxPoint); 
  }
}
//Добавляем новеньких
 var count=studentsAndPoints.length; 
studentsAndPoints.push ('Николай Фролов', 0, 'Олег Боровой', 0);
  var count_new=studentsAndPoints.length; 
  console.log('Добавлены студенты:');
for (var i=count; i<count_new; i=i+2) {
  console.log('Студент '+studentsAndPoints[i]+', '+studentsAndPoints[i+1]+' баллов');
}
//Добавляем баллы
var newPoints=['Антон Павлович', 'Николай Фролов'];
  console.log('Следующим студентам начислены баллы:');
for (var i=0; i<newPoints.length; i++) {
  nPoint=(studentsAndPoints.indexOf(newPoints[i]))+1;
  var nPointCount=studentsAndPoints[nPoint];
  var summPoint=nPointCount+10;
  studentsAndPoints.splice(nPoint, 1, summPoint);
  console.log('Студенту '+studentsAndPoints[nPoint-1]+' начислено 10 баллов. Всего:'+summPoint+' баллов.')
}
//Подводим итог
  console.log('ИТОГО баллов у студентов:');
for (var i=0; i<studentsAndPoints.length; i=i+2) {
  console.log('Студент '+studentsAndPoints[i]+' набрал '+studentsAndPoints[i+1]+' баллов');
}
//Выводим тех, у кого баллов 0
console.log('Студенты не набравшие баллов:');
var nullPoint=0;  
for (var i=1; i<studentsAndPoints.length; i=i+2) {
  if (studentsAndPoints[i]==nullPoint){  
    console.log('Студент '+studentsAndPoints[i-1] +' имеет балл '+studentsAndPoints[i]); 
  }
}
//Удаляем все кто имеет 0 баллов
console.log('Из масива удалены студенты, не набравшие баллы:');
var nullPoint=0; 
for (var i=-1; i<=studentsAndPoints.length; i=i+2) {
  if (studentsAndPoints[i]==nullPoint){
    var delStud=i-1;
    console.log('Студент '+studentsAndPoints[i-1] +' удален'); 
    studentsAndPoints.splice(delStud, 2); 
  }
}
//Список уцелевших
console.log('Списки студентов:');
for (var i=0; i<studentsAndPoints.length; i=i+2) {
  console.log('Студент '+studentsAndPoints[i]+' остался в списке, т.к. имеет '+studentsAndPoints[i+1]+' баллов');
}
//Списки передовиков с наибольшим количеством баллов
var maxRat=-1, max;  
for (var i=-1; i<studentsAndPoints.length; i=i+2) {
  if (maxRat<0||studentsAndPoints[i]>=max){
    maxRat=i; 
    max=studentsAndPoints[i];
  }
}
console.log('Студент набравший максимальный балл:')
var  maxPoint, student, maxRating=[];  
for (var i=-1; i<studentsAndPoints.length; i=i+2) {
  if (studentsAndPoints[i]==max){
    maxPoint=studentsAndPoints[i];
    student=studentsAndPoints[i-1]; 
    maxRating.push (student,maxPoint);  
    console.log('Студент '+student  +' имеет максимальный балл '+maxPoint); 
  }
}
console.log(studentsAndPoints);